# -*- coding: utf-8 -*-
"""
Formularios para la app globales
"""
# Librerias Standard
import datetime
import re

# Librerias Django
from django import forms
# Django Library
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.forms import (EmailInput, ModelForm, NumberInput, RadioSelect,
                          Select, TextInput)

# Librerias en carpetas locales
from .models import Persona

# Librerias desarrolladas por mi




class PerfilForm(ModelForm):
    """Clase para actualizar el perfil del usuario en el sistema
    """
    class Meta:
        model = Persona
        fields = (
            'avatar',
            'first_name',
            'otros_nombres',
            'last_name',
            'otros_apellidos',
            'email_secundario',
            'letra_cedula_identidad',
            'cedula_identidad',
            'telefono',
            'celular',
            'fecha_nacimiento',
            'sexo',
            'estado_civil',
            'domicilio',
            'grado_instruccion',
        )
        widgets = {
            'first_name': TextInput(attrs={'class': 'form-control'}),
            'otros_nombres': TextInput(attrs={'class': 'form-control'}),
            'last_name': TextInput(attrs={'class': 'form-control'}),
            'otros_apellidos': TextInput(attrs={'class': 'form-control'}),
            'email_secundario': TextInput(attrs={'class': 'form-control'}),
            'letra_cedula_identidad': Select(attrs={'class': 'form-control'}),
            'cedula_identidad': NumberInput(attrs={'class': 'form-control'}),
            'telefono': TextInput(attrs={'class': 'form-control'}),
            'celular': TextInput(attrs={'class': 'form-control'}),
            'fecha_nacimiento': TextInput(attrs={'class': 'form-control'}),
            'sexo': Select(attrs={'class': 'form-control'}),
            'estado_civil': Select(attrs={'class': 'form-control'}),
            'domicilio': TextInput(attrs={'class': 'form-control'}),
            'grado_instruccion': Select(attrs={'class': 'form-control'}),
        }

    # def clean(self):
    #     diccionario_limpio = self.cleaned_data
    #     fecha_nacimiento = diccionario_limpio.get('fecha_nacimiento')

    #     #Validamos que la persona tiene menos de diez y ocho(18) años
    #     fecha_actual = datetime.date.today()
    #     if fecha_nacimiento == None:
    #         raise forms.ValidationError("Campo requerido...")
    #     return fecha_nacimiento

    def clean_telefono(self):
        """
        Validamos que el teléfono cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        telefono = diccionario_limpio.get('telefono')
        patron = re.compile('^\+58\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$')
        if telefono:
            if patron.match(telefono) is None:
                raise forms.ValidationError("El número de teléfono local debe\
                                                cumplir con la forma +58 (999)\
                                                999-99-99")
        return telefono

    def clean_celular(self):
        """
        Validamos que el celular cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        celular = diccionario_limpio.get('celular')
        patron = re.compile('^\+58\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$')
        if celular:
            if patron.match(celular) is None:
                raise forms.ValidationError(
                    "El número de teléfono celular debe cumplir con la forma +58 (999) 999-99-99")
        return celular

class PersonaChangeForm(UserChangeForm):
    """Para algo sera esto
    """
    class Meta(UserChangeForm.Meta):
        model = Persona
        fields = (
            'username',
            'is_superuser',
            'is_staff',
            'is_active',
            'last_login',
            'date_joined',
            'first_name',
            'last_name',
        )


class PersonaCreationForm(UserCreationForm):
    """Con esta clase de formulario se renderiza la plantilla de registro de ususarios
    """
    class Meta(UserCreationForm.Meta):
        model = Persona
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
        )
        widgets = {
            'first_name': TextInput(attrs={'class': 'form-control', 'placeholder':'Primer nombre'}),
            'last_name': TextInput(attrs={'class': 'form-control', 'placeholder':'Primer apellido'}),
            'username': TextInput(attrs={'class': 'form-control', 'placeholder':'Usuario'}),
            'email': EmailInput(attrs={'class': 'form-control', 'placeholder':'Coreo electrónico'}),
        }

    def clean_username(self):
        """
        Validamos que el username cumpla con el formato
        """
        username = self.cleaned_data['username']
        if len(username) < 5:
            raise forms.ValidationError(
                "El campo nombre de usuario debe contener al menos de 5 dígitos.")
        return username

    def clean_first_name(self):
        """
        Validamos que el first_name cumpla con el formato
        """
        first_name = self.cleaned_data['first_name']
        if first_name.isalpha():
            return first_name
        else:
            raise forms.ValidationError(
                "El campo nombre no puede contener dígitos.")

    def clean_last_name(self):
        """
        Validamos que el last_name cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        last_name = diccionario_limpio.get('last_name')
        if last_name.isalpha():
            return last_name
        else:
            raise forms.ValidationError(
                "El campo apellido no puede contener dígitos.")

