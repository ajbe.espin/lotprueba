"""
    Definición de las URI's para la aplicación globales
"""
# Librerias Django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import (LoginView, LogoutView,
                                       PasswordResetConfirmView,
                                       PasswordResetDoneView,
                                       PasswordResetView)
from django.urls import path, reverse_lazy
from django.views.generic import TemplateView

# Librerias en carpetas locales
from .views import (HomeView, PerfilDetalle, PerfilPersona, PersonalList, SaimeAutocomplete,
                    activar, cambio_clave, Perfil, Registro, ZonaPostalAutocomplete, PerfilDelete)

from recaudacion.models import TipoTramite

app_name = 'cuenta'

urlpatterns = [
    ### Index General ###

    path('home',
        login_required(HomeView.as_view()),
        name = 'home'
    ),

    ### Registro Usuario ###
    path('registro', Registro.as_view(), name = 'registro'),
    path('activar/<uidb64>/<token>', activar, name='activar'),

    ### Login - Logout ####
    path('',
        LoginView.as_view(
            template_name='perfil_login.html',
            redirect_field_name='next'
        ),
        name='login'
    ),

    path('salir', LogoutView.as_view(next_page='cuenta:login'), name='logout'),

    ### Perfil - Cambio Clave ###
    
    path('perfil/', login_required(Perfil.as_view()), name='perfil'),

    path('cambiar-clave', login_required(cambio_clave), name='cambiar-clave'),

    ### Reinicio Clave ###
    path('reiniciar-clave-enviado',
         PasswordResetDoneView.as_view(
            template_name='reinicio_enviado.html'
         ),
        name='reiniciar-enviado'
    ),
    path('reiniciar-clave',
        PasswordResetView.as_view(
            template_name='reinicio_clave.html',
            email_template_name='reinicio_correo.html',
            success_url=reverse_lazy('cuenta:reiniciar-enviado'),
            extra_context = {'titulo':'Recuperar contraseña'},
        ),
        name='reiniciar-clave'
    ),
    path('nueva-clave/<uidb64>/<token>',
        PasswordResetConfirmView.as_view(
            template_name='reinicio_nueva_clave.html',
            success_url=reverse_lazy('cuenta:perfil'),
            post_reset_login=True,
        ),
        name='nueva-clave'
    ),

    #################################################33

    path('personal/',
        login_required(PersonalList.as_view()),
        name = 'listado_personal'
    ),

    path('perfil/<int:pk>/',
        login_required(PerfilPersona.as_view()),
        name = 'perfil_persona'
    ),

    path('perfil-detalle/<int:pk>/',
        login_required(PerfilDetalle.as_view()),
        name = 'perfil_detalle'
    ),

    path('perfil-delete/<int:pk>/',
        login_required(PerfilDelete.as_view()),
        name = 'perfil_delete'
    ),


########################################################


    path('ac/persona/',
        login_required(SaimeAutocomplete.as_view()),
        name = 'autocomplete_persona'),

    path('ac/zona_postal/',
        login_required(ZonaPostalAutocomplete.as_view()),
        name = 'autocomplete_zona_postal'),

]
