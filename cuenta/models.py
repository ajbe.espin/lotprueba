# -*- coding: utf-8
"""
Modelo de datos de la app globales
"""
# Librerias Future
from __future__ import unicode_literals

# Librerias Standard
import os

# Librerias Django
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

# Librerias en carpetas locales
from .sobreescribir_avatar import SobreEscribirAvatar


def image_path(instance, filename):
    return os.path.join('avatar', str(instance.pk) + '.' + filename.rsplit('.', 1)[1])


class Persona(AbstractUser):
    SEXO_CHOICES = (
        ('F', 'FEMENINO'),
        ('M', 'MASCULINO'),
    )
    LETRACEDULA_CHOICES = (
        ('V', 'V'),
        ('E', 'E'),
    )
    EDO_CIVIL = (
        ('S', 'SOLTERO'),
        ('C', 'CASADO'),
        ('D', 'DIVORCIADO'),
        ('V', 'VIUDO'),
    )
    GRADO_INSTRUCCION = (
        ('BACH', 'BACHILLER'),
        ('TSU', 'TÉCNICO SUPERIOR UNIVERSITARIO'),
        ('LIC', 'LICENCIADO'),
        ('ING', 'INGENIERO'),
        ('DOC', 'DOCTOR'),
    )
    password = models.CharField("Clave", max_length=128)
    last_login = models.DateTimeField("Último inicio de sesión:", default=timezone.now)
    is_superuser = models.BooleanField("Super Administrador", default=False, db_index=True)
    is_staff = models.BooleanField("Mantenimiento", default=False, db_index=True)
    is_active = models.BooleanField("Activo", default=True)
    username = models.CharField("Nombre de usuario", max_length=150, db_index=True, unique=True)
    first_name = models.CharField("Nombre", max_length=30)
    last_name = models.CharField("Apellido", max_length=30)
    email = models.CharField("Correo Electrónico", max_length=254, null=False, db_index=True, unique=True)
    email_secundario = models.CharField("Correo Secundario", max_length=254, null=True, blank=True, db_index=True, unique=True)
    letra_cedula_identidad = models.CharField('Letra C.I.', max_length=1, choices=LETRACEDULA_CHOICES, default=LETRACEDULA_CHOICES[0][0], blank=True, null=True)
    cedula_identidad = models.IntegerField("Cédula de Identidad", blank=True, null=True, db_index=True)
    otros_nombres = models.CharField("Otros Nombres", max_length=90, null=True, blank=True)
    otros_apellidos = models.CharField("Otros Apellidos", max_length=255, null=True, blank=True)
    telefono = models.CharField("Teléfono Local", max_length=255, blank=True, null=True)
    celular = models.CharField("Teléfono Celular", max_length=255, blank=True, null=True)
    fecha_nacimiento = models.DateField("Fecha de Nacimiento", blank=True, null=True)
    sexo = models.CharField(max_length=255, choices=SEXO_CHOICES, blank=True, null=True)
    domicilio = models.CharField(max_length = 255, blank=True, null=True)
    estado_civil = models.CharField(max_length = 1, choices = EDO_CIVIL, blank=True, null=True)
    grado_instruccion = models.CharField(max_length = 4, choices = GRADO_INSTRUCCION, blank=True, null=True)
    avatar = models.ImageField(max_length=255, storage=SobreEscribirAvatar(), upload_to=image_path, blank=True, null=True, default='avatar/default_avatar.png')
    
    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)
    
    @property
    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)
    
    @property
    def get_short_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    class Meta:
        verbose_name = ('Persona')
        verbose_name_plural = ('Personas')
        db_table = 'auth_user'


# @receiver(pre_save, sender=Persona)
# def actualizar_username(sender, instance, *args, **kwargs):
#     """Con esta funcin le asignamos al username del modelo user la primera
#     parte del correo electronico del usuario recien creado
#     """
#     instance.username = instance.email[:instance.email.find('@')]

class Saime(models.Model):
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    letra_cedula_identidad = models.CharField(max_length=1)
    cedula_identidad = models.IntegerField(unique=True)
    otros_nombres = models.CharField(max_length=255, blank=True, null=True)
    otros_apellidos = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    @property
    def get_short_name(self):
	    return '%s %s' % (self.first_name, self.last_name)

    @property
    def get_full_name(self):
        return '%s %s %s %s'%(self.first_name,self.otros_nombres,self.last_name ,self.otros_apellidos)
    
    class Meta:
        db_table = 'public\".\"saime'
        verbose_name = "Registro Saime"
        verbose_name_plural = "Registros del Saime"
        # ordering = ['-id']
        indexes = [
        models.Index(fields=['cedula_identidad',]),
        ]

class Estado(models.Model):
    idestado = models.CharField(db_column='id_estado', max_length=2)
    nombreestado = models.CharField(db_column='nombre_estado', max_length=255)
    idestadoglobal = models.AutoField(
        db_column='id_estado_global', primary_key=True)

    def __str__(self):
        return self.nombreestado

    class Meta:
        db_table = 'public\".\"estado'
        verbose_name = "Estado"
        verbose_name_plural = "Estados"

class Municipio(models.Model):
    idmunicipiocompleto = models.CharField(db_column='id_municipio_completo', max_length=4)
    idestado = models.CharField(Estado, db_column='id_estado', max_length=2)
    idmunicipio = models.CharField(db_column='id_municipio', max_length=2)
    nombremunicipio = models.CharField(db_column='nombre_municipio', max_length=255)
    idmunicipioglobal = models.AutoField(db_column='id_municipio_global', primary_key=True)
    idestadoglobal = models.ForeignKey(Estado, on_delete=models.PROTECT, db_column='id_estado_global')

    def __str__(self):
        return self.nombremunicipio

    class Meta:
        db_table = 'public\".\"municipio'
        verbose_name = "Municipio"
        verbose_name_plural = "Municipios"

class Parroquia(models.Model):
    idparroquiacompleto = models.CharField(db_column='id_parroquia_completo', max_length=6)
    idmunicipiocompleto = models.CharField(Municipio, db_column='id_municipio_completo', max_length=4)
    idestadoparroquia = models.CharField(db_column='id_estado', max_length=2)
    idmunicipioparroquia = models.CharField(db_column='id_municipio_parroquia', max_length=2)
    idparroquia = models.CharField(db_column='id_parroquia', max_length=2)
    nombreparroquia = models.CharField(db_column='nombre_parroquia', max_length=255)
    idparroquiaglobal = models.AutoField(db_column='id_parroquia_global', primary_key=True)
    idmunicipioglobal = models.ForeignKey(Municipio, on_delete=models.PROTECT, db_column='id_municipio_global')

    def __str__(self):
        return self.nombreparroquia

    class Meta:
        db_table = 'public\".\"parroquia'
        verbose_name = "Parroquia"
        verbose_name_plural = "Parroquias"

class ZonaPostal(models.Model):
    estado = models.CharField(max_length = 50)
    zona = models.CharField(max_length = 255)
    codigo_postal = models.IntegerField()

    def __str__(self):
        return self.zona

    class Meta:
        db_table = 'public\".\"zona_postal'
        verbose_name = "Zona Postal"
        verbose_name_plural = "Zonas Postales"
        ordering = ['-id']