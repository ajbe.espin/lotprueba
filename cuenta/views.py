# -*- coding: utf-8 -*-
"""
Vistas de la aplicación globales
"""

# Librerias de terceros
import requests
from dal import autocomplete
# Librerias Django
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views.generic import DetailView, ListView, TemplateView, CreateView, UpdateView, DeleteView

from django.contrib.messages.views import SuccessMessageMixin

from empresa.functions import *

from django.urls import reverse_lazy 

# Librerias en carpetas locales
from .forms import PerfilForm, PersonaCreationForm
from .models import Persona, Saime, ZonaPostal
from .tokens import account_activation_token


class Registro(SuccessMessageMixin, CreateView):
    model = Persona
    form_class = PersonaCreationForm
    template_name = 'registro_usuario.html'
    success_url = reverse_lazy('cuenta:login')
    success_message = 'Registro Satisfactorio, recuerde completar su perfil \
                        en la pestaña con el mismo nombre\
                        en el index de la app.'


def activar(request, uidb64, token):
    """Esta función activa a la persona cuando confirma el link enviado desde
    su correo
    """
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = Persona.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, Persona.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return redirect('cuenta:perfil')

    return render(request, 'registro_invalido.html')

class Perfil(SuccessMessageMixin,UpdateView):
    model = Persona
    form_class = PerfilForm
    template_name = 'perfil_usuario.html'
    success_url = reverse_lazy('cuenta:perfil')
    success_message = '¡La actualización de los datos de su perfil se proceso exitosamente!'

    def get_object(self, queryset = None):
        return self.model.objects.get(id = self.request.user.pk)
    
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST, request.FILES, instance = self.object)
        if form.is_valid():
            usuario = form.save(commit = False)
            usuario.save()
            return super().post(request, *args, **kwargs)
        else:
            return self.form_invalid(form)



def cambio_clave(request):
    """Esta función es para el cambio de clave del usuario
    """
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Importante!
            messages.success(request, '¡Su cambio de clave se proceso exitosamente!')
            return redirect('cuenta:perfil')
        else:
            messages.error(request, 'Su intento de cambio de clave tiene \
                errores. Por favor corrijalos.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'perfil_cambio_clave.html', {'form': form})

####################################################################################################################

class HomeView(TemplateView):
    template_name = 'index.html'
    extra_context = {'titulo':'Home',
                    'sub_titulo':' Software de control, fiscalización y \
                    recaudación para la Lotería de Caracas',
                    'sub_titulo2':'Servicio Desconcentrado de la Lotería de Caracas',
                    'fecha':fecha(),
                    'object':{'localidad':'Direccion: Av. San Martín, Centro Comercial San Martín, Nivel 2 Local 20-17, Urb. Las Américas. Caracas, Área Metropolitana CCS',
                            'telefono':'Telefono: (212) 461-37-18 / 461-59-12 ',
                            'email':'em@il: loteriadecaracas@gmail.com',
                            },}
    
            
class PersonalList(ListView):
    model = Persona
    template_name = 'rrhh/listado.html'
    context_object_name = 'personal'
    


class PerfilPersona(Perfil):
    template_name = 'rrhh/HojaVida.html'
    extra_context = {'titulo':'Datos Funcionario SDLC'}

    def get_object(self, queryset = None):
        return Persona.objects.get(id = self.kwargs['pk'])
    
    def get_success_url(self):
        return reverse_lazy('cuenta:perfil_persona', kwargs = {'pk': self.kwargs['pk']})

class PerfilDetalle(DetailView):
    model = Persona
    template_name = 'rrhh/modal_detalle.html'
    extra_context = {'titulo':'Detalle del trabajador ','bg_header':'blue','btn_color':'blue','accion':'check','botton':'Ok'}

class PerfilDelete(DeleteView):
    model = Persona
    template_name = 'rrhh/modal_delete.html'
    success_url = reverse_lazy('cuenta:listado_personal')
    extra_context = {'titulo': 'Eliminación de datos de Personal', 'bg_header':'red','btn_color':'danger','accion':'trash','boton':'Eliminar'}

    def delete(self, request, *args, **kwargs):
        self.get_object().delete()
        messages.success(request,'Se ha hecho la eliminación de este trabajador')
        
        return super().delete(request, *args, **kwargs)

    ########################################################################################

class SaimeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Saime.objects.none()
        if self.q:
            qs = Saime.objects.filter(cedula_identidad= self.q)
        return qs

class ZonaPostalAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = ZonaPostal.objects.all()
        if self.q:
            qs = ZonaPostal.objects.filter(software__icontains = self.q)
        return qs