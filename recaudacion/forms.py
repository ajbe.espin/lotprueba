import re

from dal import autocomplete
from django import forms
from django.forms import (CheckboxInput, EmailInput, ModelForm, NumberInput,
                          Select, TextInput, CharField)

from cuenta.models import Parroquia
from empresa.models import Empresa
from .models import *
from empresa.functions import *


class TramiteForm(forms.ModelForm):    
    class Meta:
        model = Tramite
        fields = [
            'empresa',
            'tipo_tramite',
            'metodo_pago',
            'banco',
            'nro_bauche',
            'monto_bauche',
            'fecha_bauche',
            'parroquia',
            'tramite',
            'status_retiro',
            'status_actividad',
        ]
        widgets = {
            'empresa':autocomplete.ModelSelect2(url='empresa:autocomplete_empresa', attrs={'data-placeholder': '..... Nro de RIF .....', 'class': 'form-control',  'style':'width:100%'},),
            'tipo_tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_licencia', attrs={'class': 'form-control','data-placeholder': '..... TIPO DE TRÁMITE .....', 'style':'width:100%'},),
            'metodo_pago':autocomplete.ModelSelect2(url='recaudacion:autocomplete_metodo_pago', attrs={'class': 'form-control','data-placeholder': '..... MÉTODO DE PAGO .....', 'style':'width:100%'},),
            'banco':autocomplete.ModelSelect2(url='recaudacion:autocomplete_banco', attrs={'class': 'form-control','data-placeholder': '..... ENTIDAD BANCARIA .....', 'style':'width:100%'},),
            'nro_bauche':forms.TextInput(attrs={'class':'form-control'}),
            'monto_bauche':NumberInput(attrs={'class':'form-control'}),
            'fecha_bauche':forms.TextInput(attrs={'class':'form-control datepicker'}),
            'parroquia':autocomplete.ModelSelect2(url='recaudacion:autocomplete_parroquia', attrs={'class': 'form-control','data-placeholder': '..... PARROQUIA .....', 'style':'width:100%'},),
            'tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_inscripciones',forward = ['empresa'], attrs={'class': 'form-control','data-placeholder': '..... INSCRIPCIÓN ÚNICA .....', 'style':'width:100%'},),
            'status_retiro':CheckboxInput(attrs = {'class':'minimal'}),
            'status_actividad':CheckboxInput(attrs = {'class':'minimal'}), 
        }

    def clean_empresa(self):
        empresa = self.cleaned_data['empresa']
        if empresa == None:
            raise forms.ValidationError(
                "Se debe ingresar el RIF del Sujeto pásivo, este campo no puede ir vacio.")
        return empresa
    
    def clean_metodo_pago(self):
        metodo_pago = self.cleaned_data['metodo_pago']
        if metodo_pago == None:
            raise forms.ValidationError(
                "Usted debe ingresar el método de pago con el cual cancelará el trámite, este campo no puede ir vacio.")
        return metodo_pago

    def clean_banco(self):
        banco = self.cleaned_data['banco']
        if banco == None:
            raise forms.ValidationError(
                "Usted debe ingresar la entidad bancaria en la cual canceló el trámite, este campo no puede ir vacio.")
        return banco

    def clean_nro_bauche(self):
        nro_bauche = self.cleaned_data['nro_bauche']
        if nro_bauche == None or len(nro_bauche) < 6:
            raise forms.ValidationError(
                "Usted debe ingresar un mínimo de 6 dígitos para identificar el número de transacción realizadad, este campo no puede ir vacio.")
        return nro_bauche

    def clean_monto_bauche(self):
        monto_bauche = self.cleaned_data['monto_bauche']

        if monto_bauche == 0:
            raise forms.ValidationError(
                "Este campo no puede ir vacio o ser cero (0), ingrese el monto correspondiente al costo del trámite solicitado.")
        return monto_bauche

    def clean_fecha_bauche(self):
        fecha_bauche = self.cleaned_data['fecha_bauche']
        hoy = var = fecha()['hoy'].date()
        if fecha_bauche > hoy:
            raise forms.ValidationError(
                "Usted no puede ingresar un comprobante de pago con una fecha mayor al del día de hoy.")
        elif fecha_bauche == None:
            raise forms.ValidationError(
                "Debe ingresar una fecha para poder registrar el pago de un trámite.")
        return fecha_bauche


class EditTramite(forms.ModelForm):
    class Meta:
        model = Tramite
        fields = [
            'parroquia',
            'tramite',
            'status_retiro',
            'status_actividad',
        ]
        widgets = {
            'parroquia':autocomplete.ModelSelect2(url='recaudacion:autocomplete_parroquia', attrs={'class': 'form-control','data-placeholder': '..... PARROQUIA .....', 'style':'width:100%'},),
            'tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_inscripciones', attrs={'class': 'form-control','data-placeholder': '..... INSCRIPCIÓN ÚNICA .....', 'style':'width:100%'},),
            'status_retiro':CheckboxInput(attrs = {'class':'minimal'}),
            'status_actividad':CheckboxInput(attrs = {'class':'minimal'}),
            
        }

    def clean_parroquia(self):
        parroquia = self.cleaned_data['parroquia']
        if parroquia == None:
            raise forms.ValidationError(
                "Usted debe ingresar la parroquia donde tendrá ámbito de ación el trámite, este campo no puede ir vacio.")
        return parroquia


class RenovacionForm(forms.ModelForm):    
    class Meta:
        model = Tramite
        fields = [
            'empresa',
            'tipo_tramite',
            'metodo_pago',
            'banco',
            'nro_bauche',
            'monto_bauche',
            'fecha_bauche',
            'tramite',
        ]
        widgets = {
            'empresa':autocomplete.ModelSelect2(url='empresa:autocomplete_empresa', attrs={'data-placeholder': '..... Nro de RIF .....', 'class': 'form-control',  'style':'width:100%'},),
            'metodo_pago':autocomplete.ModelSelect2(url='recaudacion:autocomplete_metodo_pago', attrs={'class': 'form-control','data-placeholder': '..... MÉTODO DE PAGO .....', 'style':'width:100%'},),
            'tipo_tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_tipo_tramite_licencias', attrs={'class': 'form-control','data-placeholder': '..... TIPO DE TRÁMITE .....', 'style':'width:100%'},),
            'banco':autocomplete.ModelSelect2(url='recaudacion:autocomplete_banco', attrs={'class': 'form-control','data-placeholder': '..... ENTIDAD BANCARIA .....', 'style':'width:100%'},),
            'nro_bauche':forms.TextInput(attrs={'class':'form-control'}),
            'monto_bauche':NumberInput(attrs={'class':'form-control'}),
            'fecha_bauche':forms.TextInput(attrs={'class':'form-control datepicker'}),
            'tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_renovaciones',forward = ['empresa'], attrs={'class': 'form-control','data-placeholder': '..... LICENCIA SDLC .....', 'style':'width:100%'},),
            
        }

    def clean_empresa(self):
        empresa = self.cleaned_data['empresa']
        if empresa == None:
            raise forms.ValidationError(
                "Se debe ingresar el RIF del Sujeto pásivo, este campo no puede ir vacio.")
        return empresa
    
    def clean_metodo_pago(self):
        metodo_pago = self.cleaned_data['metodo_pago']
        if metodo_pago == None:
            raise forms.ValidationError(
                "Usted debe ingresar el método de pago con el cual cancelará el trámite, este campo no puede ir vacio.")
        return metodo_pago

    def clean_banco(self):
        banco = self.cleaned_data['banco']
        if banco == None:
            raise forms.ValidationError(
                "Usted debe ingresar la entidad bancaria en la cual canceló el trámite, este campo no puede ir vacio.")
        return banco

    def clean_nro_bauche(self):
        nro_bauche = self.cleaned_data['nro_bauche']
        if nro_bauche == None or len(nro_bauche) < 6:
            raise forms.ValidationError(
                "Usted debe ingresar un mínimo de 6 dígitos para identificar el número de transacción realizadad, este campo no puede ir vacio.")
        return nro_bauche

    def clean_monto_bauche(self):
        monto_bauche = self.cleaned_data['monto_bauche']

        if monto_bauche == 0:
            raise forms.ValidationError(
                "Este campo no puede ir vacio o ser cero (0), ingrese el monto correspondiente al costo del trámite solicitado.")
        return monto_bauche

    def clean_fecha_bauche(self):
        fecha_bauche = self.cleaned_data['fecha_bauche']
        hoy = fecha()['hoy'].date()
        if fecha_bauche > hoy:
            raise forms.ValidationError(
                "Usted no puede ingresar el número de un bauche con una fecha mayor al del día de hoy.")
        elif fecha_bauche == None:
            raise forms.ValidationError(
                "Debe ingresar una fecha para poder registrar el pago de un trámite.")
        return fecha_bauche



class AporteForm(forms.ModelForm):
    nro_semana = CharField( widget = TextInput(attrs={'type':'number', 'class':'form-control'}))
    class Meta:
        model = Tramite
        fields = [
            'empresa',
            'tipo_tramite',
            'metodo_pago',
            'banco',
            'nro_bauche',
            'monto_bauche',
            'fecha_bauche',
            'tramite',
        ]
        widgets = {
            'empresa':autocomplete.ModelSelect2(url='empresa:autocomplete_empresa', attrs={'data-placeholder': '..... Nro de RIF .....', 'class': 'form-control',  'style':'width:100%'},),
            'metodo_pago':autocomplete.ModelSelect2(url='recaudacion:autocomplete_metodo_pago', attrs={'class': 'form-control','data-placeholder': '..... MÉTODO DE PAGO .....', 'style':'width:100%'},),
            'tipo_tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_tipo_aportes', attrs={'class': 'form-control','data-placeholder': '..... TIPO DE TRÁMITE .....', 'style':'width:100%'},),
            'banco':autocomplete.ModelSelect2(url='recaudacion:autocomplete_banco', attrs={'class': 'form-control','data-placeholder': '..... ENTIDAD BANCARIA .....', 'style':'width:100%'},),
            'nro_bauche':forms.TextInput(attrs={'class':'form-control'}),
            'monto_bauche':NumberInput(attrs={'class':'form-control'}),
            'fecha_bauche':forms.TextInput(attrs={'class':'form-control datepicker'}),
            'tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_aportes',forward = ['empresa'], attrs={'class': 'form-control','data-placeholder': '..... LICENCIA SDLC .....', 'style':'width:100%'},),
            
        }

class AgenciaForm(forms.ModelForm):
    empresa = forms.ModelChoiceField(
        queryset=Empresa.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='empresa:autocomplete_empresa',
            attrs={'data-placeholder': ' Nro de RIF ','class': 'form-control','style':'width:100%'},
        ),
        
    )

    class Meta:
        model = Agencia
        fields = [
            'tramite',
            'direccion',
            'nro_maquinas',
        ]
        widgets = {
            'tramite':autocomplete.ModelSelect2(url='recaudacion:autocomplete_licencias',forward = ['empresa'], attrs={'class': 'form-control','data-placeholder': '..... LICENCIA SDLC .....', 'style':'width:100%'},),
            'direccion':forms.TextInput(attrs={'class':'form-control'}),
            'nro_maquinas':NumberInput(attrs={'class':'form-control'}),
        }