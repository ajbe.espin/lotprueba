from dal import autocomplete
from django.contrib.auth.decorators import login_required, permission_required
from django.urls import path

from .views import *

app_name = 'recaudacion'

urlpatterns = [
############################    PAGOS   ############################


#########################################################
####################### Trámites ########################
#########################################################



    path('tramites/',
        login_required(Listados.as_view(
            model = Tramite,
            template_name = 'Tramite/listado.html',
            extra_context = {'titulo':'Trámites SDLC','sub_titulo':'Listado General'},
            filtro_por = 'tipo_tramite',
            context_object_name = 'tramites',
        )),
        name = 'list_tramites'),

    path('tramite/emision/<slug:tramite>/<slug:template>/',
        login_required(EmiteTramite.as_view(
            model = Tramite,
            # template_name = 'Tramite/tramite_create.html',
            success_url = reverse_lazy('recaudacion:list_tramites'),
            form_class  = TramiteForm,
            success_message = 'Se ha generado un trámite de manera éxitosa.',
            extra_context = {'titulo':'Formulario de Trámites', 'accion':'save', 'btn_color':'primary', 'boton':'Guardar'}
        )),
        name = 'tramite_emision'),

    path('tramite/detail/<int:pk>',
        login_required(DetailView.as_view(
            model = Tramite,
            template_name = 'Tramite/modal_detalleTramite.html',
            extra_context = {'titulo':'Detalle del Trámite Nro.', 'boton':'Ok', 'accion':'check','bg_header':'navy','btn_color':'info'}
        )),
        name = 'detail_inscripcion'),



    path('tramite/edit/<int:pk>/',
        login_required(EditaTramite.as_view(
            model = Tramite,
            form_class = EditTramite,
            success_message = 'Se ha editado de manera satisfactoriamente su Trámite.',
            template_name = 'Tramite/modal_edit_tramite.html',
            success_url = reverse_lazy('recaudacion:list_tramites'),
            extra_context = {'titulo':'Edición de Trámite', 'boton':'Editar', 'accion':'refresh','bg_header':'blue','btn_color':'primary'}
        )),
        name = 'tramite_edit'),

    
    path('renovacion/<slug:tramite>/<slug:template>',
        login_required(RenovacionTramite.as_view(
            model = Tramite,
            success_url = reverse_lazy('recaudacion:list_tramites'),
            form_class  = RenovacionForm,
            success_message = 'Se ha generado la renovación de la Licencia SDLC de manera éxitosa.',
            extra_context = {'titulo':'Trámite','sub_titulo':'Renovación de Licencia SDLC', 'accion':'save', 'btn_color':'primary', 'boton':'Guardar'}
        )),
        name = 'renovacion'),

    path('aporte/<slug:tramite>/<slug:template>',
        login_required(RenovacionTramite.as_view(
            model = Tramite,
            success_url = reverse_lazy('recaudacion:list_tramites'),
            form_class  = AporteForm,
            success_message = 'Se ha generado un aporte semanal sobre la Licencia SDLC de manera éxitosa.',
            extra_context = {'titulo':'Trámite','sub_titulo':'Aporte Semanal de Licencias', 'accion':'save', 'btn_color':'primary', 'boton':'Guardar'}
        )),
        name = 'aporte'),

    path('agencias/',
        login_required(ListView.as_view(
            model = Agencia,
            template_name = 'agencia/listado.html',
            extra_context = {'titulo':'Agencias SDLC','sub_titulo':'Listado General','fecha':fecha()},
            context_object_name = 'agencias',
        )), name = 'agencias'),

    path('agencias/create/',
        login_required(AgenciaCreate.as_view(
            model = Agencia,
            form_class = AgenciaForm,
            template_name = 'agencia/modal_create.html',
            success_url = reverse_lazy('recaudacion:agencias'),
            extra_context = {'titulo':'Formulario de Agencias', 'accion':'save', 'btn_color':'primary', 'boton':'Guardar'}
        )),
        name = 'agencias_create'),

#########################################################
##################### Estadisticas ######################
#########################################################
    
    path('tramites/<int:annio>/',
        login_required(EstadisticaTramite.as_view(
            model = Tramite,
            filtro_por = 'fecha_emision__year',
            extra_context = {'titulo':'Estadisticas por trámite', 'sub_titulo':'Listado General'},
            context_object_name = 'tramites',
            template_name = 'graph_tramite/grafico.html',
        )),
        name = 'estadisticas_tramite'),

    path('tramitesA/<int:annio>/',
        login_required(EstadisticaTramite.as_view(
            model = Tramite,
            extra_context = {'titulo':'Estadisticas por trámite', 'sub_titulo':'Listado General'},
            context_object_name = 'tramites',
            template_name = 'graph_tramite/modal_estadistica.html',
        )),
        name = 'estadisticas_tramite_modal'),

#########################################################
#################### AUTOCOMPLETE'S #####################
#########################################################

    path('ac/tipo_tramite',
        login_required(LicenciAutocomplete.as_view()),
        name = 'autocomplete_licencia'),

    path('ac/Banco',
        login_required(BancoAutocomplete.as_view()),
        name = 'autocomplete_banco'),
    
    path('ac/metodopago',
        login_required(MetodoPagoAutocomplete.as_view()),
        name = 'autocomplete_metodo_pago'),
    
    path('ac/parroquia',
        login_required(ParroquiaAutocomplete.as_view()),
        name = 'autocomplete_parroquia'),

    path('ac/inscripciones',
        login_required(InscripcionesAutocomplete.as_view()),
        name = 'autocomplete_inscripciones'),

    path('ac/renovaciones',
        login_required(RenovacionLicenciaAutocomplete.as_view()),
        name = 'autocomplete_renovaciones'),

    path('ac/renovacion_tipo_tramite/',
        login_required(RenovacionTipoTramiteAutocomplete.as_view()),
        name = 'autocomplete_tipo_tramite_licencias'),

    path('ac/aporte_tramite/',
        login_required(AporteTramiteAutocomplete.as_view()),
        name = 'autocomplete_aportes'),
    
    path('ac/aporte_tipo_tramite/',
        login_required(AporteTipoTramiteAutocomplete.as_view()),
        name = 'autocomplete_tipo_aportes'),

    path('ac/licencias/',
        login_required(LicenciaAutocomplete.as_view()),
        name = 'autocomplete_licencias'),

]
