from django.db import models
from django.db.models import Avg, Count, Max, Sum

# Create your models her



class MetodoPago(models.Model):
    metodopago = models.CharField(max_length = 60)
    descripcion = models.TextField(blank = True, null = True)

    def __str__(self):
        return '%s'%(self.metodopago)

    class Meta:
        db_table = 'recaudacion\".\"metodopago'
        verbose_name = "Metodopago"
        verbose_name_plural = "Metodopagos"
        ordering = ['-id']

class Banco(models.Model):
    banco = models.CharField(max_length = 30)
    siglas = models.CharField(max_length = 8)

    def __str__(self):
        return '%s'%(self.banco)

    class Meta:
        db_table = 'recaudacion\".\"banco'
        verbose_name = "Banco"
        verbose_name_plural = "Bancos"
        ordering = ['-id']

class UnidadTributaria(models.Model):
    unidad_tributaria = models.IntegerField(default = 0)
    fecha_alta = models.DateField(auto_now = False)
    fecha_baja = models.DateField(auto_now = False, null = True, blank = True)

    def __str__(self):
        return '%s'%(self.unidad_tributaria)


    class Meta:
        db_table = 'recaudacion\".\"unidad_tributaria'
        verbose_name = "Unidad Tributaria"
        verbose_name_plural = "Unidades Tributarias"
        ordering = ['-id']

class TipoTramite(models.Model):
    siglas = models.CharField(max_length = 4)
    descripcion = models.CharField(max_length = 40)

    def __str__(self):
        return '%s'%(self.descripcion)

    class Meta:
        db_table = 'recaudacion\".\"tipo_tramite'
        verbose_name = "Tipo de Trámite"
        verbose_name_plural = "Tipos de Trámites"
        ordering = ['-id']

class CostoTramite(models.Model):
    tipo_tramite = models.ForeignKey('TipoTramite', null = True, blank = True, on_delete = models.PROTECT,
                                    verbose_name = 'Tipo de Trámite', db_column = 'tipo_tramite_id')
    monto = models.IntegerField(default = 0, verbose_name = 'Monto en UT')
    fecha_alta = models.DateField(auto_now = False)
    fecha_baja = models.DateField(auto_now = False, blank = True, null = True)

    def __str__(self):
        return '%s'%(self.tipo_tramite)

    class Meta:
        db_table = 'recaudacion\".\"costo_tramite'
        verbose_name = "Costo de Trámite"
        verbose_name_plural = "Costos del Trámite"
        ordering = ['-id']



class Tramite(models.Model): 
    empresa = models.ForeignKey('empresa.Empresa', null = False, blank = False, on_delete = models.PROTECT,
                                verbose_name = 'Nro de RIF', db_column = 'empresa_id')
    tipo_tramite = models.ForeignKey('TipoTramite', null = True, blank = True, on_delete = models.PROTECT,
                                verbose_name = 'Tipo de Trámite', db_column = 'tipo_tramite_id')
    metodo_pago = models.ForeignKey('MetodoPago', null = False, blank = False, on_delete = models.PROTECT,
                                    verbose_name = 'Método de Pago', db_column = 'metodo_pago_id')
    banco = models.ForeignKey('Banco', null = False, blank = False, on_delete = models.PROTECT,verbose_name = 'Banco', db_column = 'banco_id')
    nro_bauche = models.CharField(max_length = 20, unique = True)
    monto_bauche = models.FloatField(default = 0)
    fecha_bauche = models.DateField(auto_now = False)
    tramite = models.ForeignKey('Tramite',null = True, blank = True, db_column = 'tramite_id', on_delete = models.PROTECT, related_name = 'requisito')
    nro_tramite = models.CharField(max_length = 14, null = True, blank = True, unique = True)
    fecha_emision = models.DateField(auto_now_add = True)
    fecha_retiro = models.DateField(auto_now = False, null = True, blank = True)
    fecha_vencimiento = models.DateField(auto_now = False, null = True, blank = True)
    status_vigencia = models.BooleanField(default = True,verbose_name = 'Vigente?')
    status_retiro = models.BooleanField(default = False, verbose_name = 'Retirado?')
    status_actividad = models.BooleanField(default = False, verbose_name = 'Activo?')
    status_uso = models.BooleanField(default = False)
    costo_tramite = models.FloatField(default = 0, verbose_name = 'Costo Trámite Bs')
    parroquia = models.ForeignKey('cuenta.Parroquia',null = True, blank = True, on_delete = models.PROTECT, db_column = 'parroquia_id' )
    funcionario = models.ForeignKey('cuenta.Persona', null = False, blank = False, on_delete = models.PROTECT,
                                    verbose_name = 'Funcionario', db_column = 'funcionario_id')

    def __str__(self):
        return '%s'%(self.nro_tramite)
    
    @property
    def costo_soberano(self):
        return self.monto_bauche / 100000


    class Meta:
        db_table = 'recaudacion\".\"tramite'
        verbose_name = "Tramite"
        verbose_name_plural = "Tramite"
        ordering = ['-id']


class Agencia(models.Model):
    tramite = models.ForeignKey('Tramite', null = False, blank = False, on_delete = models.PROTECT, db_column = 'licencia_id', verbose_name = 'Licencia')
    nro_maquinas = models.IntegerField()
    direccion = models.CharField(max_length = 200, null = True, blank = True)

    def __str__(self):
        return '%s'%(self.tramite.empresa)
    
    class Meta:
        db_table = 'recaudacion\".\"agencia'
        verbose_name = "Agencia"
        verbose_name_plural = "Agencia"
        ordering = ['-id']


######################################################################################
############################### VISTAS ###############################################

# class TramiteParroquia(models.Model):
#     id = models.IntegerField(primary_key = True)
#     parroquia = models.CharField(max_length = 255)
#     parroquia_id = models.IntegerField()
#     descripcion = models.CharField(max_length = 40)
#     tramite_id = models.IntegerField()
#     annio = models.IntegerField()
#     mes = models.IntegerField()
#     total_tramites = models.IntegerField()
#     recaudado_bs = models.IntegerField()
#     recaudado_bss = models.FloatField()

#     class Meta:
#         managed = False
#         db_table = 'tools\".\"tramites_parroquia_annio'


# class VistaPago(models.Model):
#     """
#     Vista para obtener información de los pagos generados de 
#     forma mensual y/o anual.
#     """
#     id = models.IntegerField(primary_key = True)
#     annio = models.IntegerField()
#     mes = models.IntegerField()
#     total_pagos = models.FloatField()
#     recaudo = models.IntegerField()
#     recaudo_s = models.FloatField()

#     class Meta:
#         managed = False
#         db_table = 'tools\".\"estadistica_pago'


# class VistaPagoParroquia(models.Model):
#     """
#     Vista para obtener información de los pagos por parroquia de 
#     forma mensual y/o anual.
#     """
#     id = models.IntegerField(primary_key = True)
#     annio = models.IntegerField()
#     mes = models.IntegerField()
#     parroquia = models.CharField(max_length = 255)
#     pagos_parroquia = models.FloatField()
#     recaudado_parroquia = models.IntegerField()
#     recaudado_parroquia_s = models.FloatField()

#     class Meta:
#         managed = False
#         db_table = 'tools\".\"pagos_parroquia'


