from dal import autocomplete
from django.db import IntegrityError, transaction

from .clases import *

from cuenta.models import Estado, Municipio, Parroquia
from empresa.functions import *

from .forms import *
from .models import *

from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import HttpResponse

# from easy_pdf.views import PDFTemplateView


def some_view(request):
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="reporte.pdf"'

    buffer = BytesIO()

    # Create the PDF object, using the BytesIO object as its "file."
    p = canvas.Canvas(buffer)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, 'No imprime las variables')

    # Close the PDF object cleanly.
    p.showPage()
    p.save()

    # Get the value of the BytesIO buffer and write it to the response.
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response


class Listados(GeneralList):


    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        filtro = self.request.GET.get('filtro')
        contexto['fecha'] = fecha()
        contexto['tramitesL'] =  TipoTramite.objects.order_by('id')     
        if filtro is None:
            contexto['titulo'] = self.extra_context['titulo']
        else:
            contexto['titulo'] = contexto['tramitesL'].get(id = filtro).descripcion
        contexto['parroquias'] = Parroquia.objects.filter(idmunicipioglobal = 1).order_by('-idmunicipioglobal')
        contexto['tramites_annio_pasado'] = self.get_queryset().filter(fecha_emision__year = contexto['fecha']['annio_pasado'])
        contexto['tramites_annio_actual'] = self.get_queryset().filter(fecha_emision__year = contexto['fecha']['annio'])

        #   Párametro para Trámites  #
        contexto['filtro'] = filtro
        contexto['listado_tramites'] = ['inscripcion','licencia', 'renovacion','aporte'] 
        contexto['templates'] = ['inscripcion_create','licencia_create','renovacion_create','aporte_create']
        return contexto


#########################################################
####################### TRAMITE'S #######################
#########################################################

class EmiteTramite(GeneralCreate):

    def get_template_names(self):
        return ['Tramite/%s.html' % self.kwargs['template']]

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['tramite'] = self.kwargs['tramite']
        
        if contexto['tramite'] == 'inscripcion':
            contexto['sub_titulo'] = 'Inscripcíon única'
        elif contexto['tramite'] == 'licencia':
            contexto['sub_titulo'] = 'Licencia SDLC'
        elif contexto['tramite'] == 'renovacion':
            contexto['sub_titulo'] = 'Renovación de Licencia SDLC'
        elif contexto['tramite'] == 'aporte':
            contexto['sub_titulo'] = 'Aportes Semanales SDLC'
        return contexto

    def form_valid(self, form):
        tramite = form.save(commit = False)
        tramite.funcionario = self.request.user
        tramite.fecha_emision = fecha()['hoy']
        if self.kwargs['tramite'] == 'inscripcion':
            self.inscripcion(form)
        elif self.kwargs['tramite'] == 'licencia':
            self.licencias(form)

        costo = costo_tramite(tramite.tipo_tramite)
        if tramite.monto_bauche > costo:
            excedente = tramite.monto_bauche - costo
            credito_fiscal(tramite.empresa, tramite.tipo_tramite.descripcion, tramite, excedente)
            messages.info(self.request, 'Se ha generado un crédito fiscal a nombre del Sujeto Pásivo.')
        return HttpResponseRedirect(self.get_success_url())

    def inscripcion(self, form):
        inscripcion = form.save(commit = False)
        insc = TipoTramite.objects.get(id = 1) # Aquí asigno el tipo de trámite
        inscripcion.tipo_tramite = insc
        costo = costo_tramite(inscripcion.tipo_tramite)
        if inscripcion.tipo_tramite.pk == 1:
            inscripcion.status_actividad = True
            inscripcion.save()
            messages.success(self.request, self.success_message)
        else:
            messages.info(self.request, 'El Pago que se acaba de seleccionar no se corresponde con el trámite seleccionado.')
            return self.form_invalid(form)

    def licencias(self, form):
        licencia = form.save(commit = False)
        if licencia.tipo_tramite.pk in [2,3,4,5]:
            licencia.tramite.status_uso = True #    Aqui inactivo la inscripcion
            licencia.tramite.save() #   Aquí guardo la modificacion sobre la inscripcion
            licencia.parroquia = licencia.tramite.parroquia
            licencia.status_actividad = True
            licencia.status_uso = True
            licencia.save()
            messages.success(self.request, self.success_message)
        else:
            messages.info(self.request, 'El Pago que se acaba de seleccionar no se corresponde con el trámite seleccionado.')
            return self.form_invalid(form)
    
    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form = form))
    

class EditaTramite(GeneralUpdate):

    def form_valid(self, form):
        if self.get_object().tipo_tramite.pk == 1:
            self.inscripcion(form)
        elif self.get_object().tipo_tramite.pk in [2,3,4,5]:
            self.licencias(form)
        return HttpResponseRedirect(self.get_success_url())

    def inscripcion(self, form):
        inscripcion = form.save(commit = False)
        if inscripcion.parroquia == None:
            self.msg_type = 'info'
            self.msg_error = 'Usted no puede modificar la parroquia de esta Inscripción SDLC y dejar a la misma vacia.'
            self.form_invalid(form)

        if inscripcion.status_retiro is True and inscripcion.fecha_retiro is None:
            inscripcion.fecha_retiro = fecha()['hoy']
        
        licencia = self.model.objects.get(tramite = self.get_object().pk)
        if self.get_object().parroquia != inscripcion.parroquia:
            if licencia:
                licencia.parroquia = inscripcion.parroquia #    Aqui modifico la parroquia a la licencia...
                licencia.save()
        elif self.get_object().status_actividad != inscripcion.status_actividad:
            if self.request.user.is_superuser:
                if licencia:
                    licencia.status_actividad = inscripcion.status_actividad #    Aqui modifico el status de actividad a la licencia...
                    licencia.save()
        inscripcion = form.save()
        messages.success(self.request, self.success_message)
    
    def licencias(self, form):
        licencia = form.save(commit = False)
        
        if licencia.parroquia != self.get_object().tramite.parroquia:
            self.msg_type = 'warning'
            self.msg_error = 'Usted no puede modificar la parroquia de esta Licencia sin antes haber modificado la Inscripción única.'
            self.form_invalid(form)
        elif licencia.parroquia == None:
            self.msg_type = 'info'
            self.msg_error = 'Usted no puede modificar la parroquia de esta Licencia y dejar esta vacia.'
            self.form_invalid(form)
        else:
            if self.get_object().tramite.status_actividad != licencia.status_actividad:
                licencia.status_actividad = self.get_object().tramite.status_actividad
                messages.info(self.request, 'Para modificar el estatus de esta Licencia debes modificar antes el estatus de la Inscripción única asociada a la misma.')
            else:
                licencia.tramite.status_uso = True #    Aqui inactivo la inscripcion
                licencia.parroquia = licencia.tramite.parroquia
                licencia.tramite.save() #   Aquí guardo la modificacion sobre la inscripcion
                
                licencia.save()
                messages.success(self.request, self.success_message)
    



# class DeleteTramite(GeneralDelete):

#     def delete(self, request, *args, **kwargs):
#         self.object = self.get_object()


class RenovacionTramite(GeneralCreate):

    def get_template_names(self):
        return ['Tramite/%s.html' % self.kwargs['template']]

    def form_valid(self, form):    
        renovacion = form.save(commit = False)
        renovacion.funcionario = self.request.user
        renovacion.parroquia = renovacion.tramite.parroquia
        renovacion.costo_tramite = costo_tramite(renovacion.tipo_tramite.pk)
        renovacion.tramite.status_vigencia = True
        renovacion.tramite.save()
        renovacion.status_actividad = True
        renovacion.status_uso = True
        renovacion.save()
        costo = costo_tramite(renovacion.tipo_tramite)
        if renovacion.monto_bauche > costo:
            excedente = renovacion.monto_bauche - costo
            credito_fiscal(renovacion.empresa, renovacion.tipo_tramite.descripcion, renovacion, excedente)
            messages.info(self.request, 'Se ha generado un crédito fiscal a nombre del Sujeto Pásivo.')
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form = form))


class AporteSemanal(GeneralCreate):

    def get_template_names(self):
        return ['Tramite/%s.html' % self.kwargs['template']]

    def form_valid(self, form):
        aporte = form.save(commit = False)
        aporte.funcionario = self.request.user
        aporte.parroquia = aporte.tramite.parroquia
        aporte.costo_tramite = self.costo_aporte(aporte.nro_semana)
        aporte.status_actividad = True
        aporte.status_uso = True
        aporte.status_vigencia = True
        print(form.nro_semana)
        aporte.save()
        costo = self.costo_aporte(aporte.nro_semana)
        if aporte.monto_bauche > costo:
            excedente = aporte.monto_bauche - costo
            credito_fiscal(renovacion.empresa, renovacion.tipo_tramite.descripcion, renovacion, excedente)
            messages.info(self.request, 'Se ha generado un crédito fiscal a nombre del Sujeto Pásivo.')
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())
    
    def costo_aporte(self, nro_semana):
        #   Este método calcula el total a cancelar por aporte dependiendo la cantidad de semanas que tenga retrasado el usuario.}
        return 9000

class AgenciaCreate(GeneralCreate):

    def form_valid(self, form):
        agencia = form.save(commit = False)
        agencia.save()
        return super().form_valid(form)

class Estadisticas(TemplateView):
    
    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['annio'] = self.kwargs['annio']
        """ Muestra la gráfica ppal """
        base = VistaPago.objects.order_by('mes')
        qsA = base.filter(annio = contexto['annio'])
        qsB = base.filter(annio = contexto['annio']-1)
        contexto['datos_actuales'] = barra_estadistica(qsA,'total_pagos')
        contexto['datos_pasados'] = barra_estadistica(qsB,'total_pagos')
        contexto['montos'] = qsA

        contexto['pagos_parroquia'] = VistaPagoParroquia.objects.filter(annio = contexto['annio'])

        contexto['pagos_tipo_tramite'] = base.filter()

        return contexto



class EstadisticaTramite(GeneralList):
   
    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['annio'] = self.kwargs['annio']
        qs = TramiteParroquia.objects.all()
        qsA = qs.filter(annio = contexto['annio'])
        qsB = qs.filter(annio = (contexto['annio'] - 1))
        contexto['datos_pasados'] = barra_estadistica(qsB,'total_tramites')
        contexto['datos_actuales'] = barra_estadistica(qsA,'total_tramites')
        return contexto

#########################################################
#################### AUTOCOMPLETE'S #####################
#########################################################



class BancoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        
        qs = Banco.objects.all()
        if self.q:
            qs = Banco.objects.filter(banco__icontains = self.q)
        return qs

class MetodoPagoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        
        qs = MetodoPago.objects.all()
        if self.q:
            qs = MetodoPago.objects.filter(metodopago__icontains = self.q)
        return qs

class LicenciAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        
        qs = TipoTramite.objects.exclude(id__in = [1,6,7,8,9,10,11,12,13])
        if self.q:
            qs = TipoTramite.objects.filter(descripcion__icontains = self.q).exclude(id__in = [1,6,7,8,9,10,11,12,13])
        return qs


class ParroquiaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        
        qs = Parroquia.objects.all()
        if self.q:
            qs = Parroquia.objects.filter(idmunicipioglobal = 1, nombreparroquia__icontains = self.q)
        return qs
        

class InscripcionesAutocomplete(autocomplete.Select2QuerySetView):
    
    def get_queryset(self):
        empresa = self.forwarded.get('empresa', None)
        qs = Tramite.objects.filter(empresa = empresa, status_uso = False, tramite = None)
        return qs

class LicenciaAutocomplete(autocomplete.Select2QuerySetView):
    
    def get_queryset(self):
        empresa = self.forwarded.get('empresa', None)
        qs = Tramite.objects.filter(empresa = empresa, tipo_tramite__in = [2,3,4,5])
        return qs

class RenovacionLicenciaAutocomplete(autocomplete.Select2QuerySetView):
    
    def get_queryset(self):
        empresa = self.forwarded.get('empresa', None)
        qs = Tramite.objects.filter(empresa = empresa, tipo_tramite__in = [2,3,4,5], status_vigencia = False)
        return qs

class RenovacionTipoTramiteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        
        qs = TipoTramite.objects.exclude(id__in = [1,2,3,4,5,10,11,12,13])
        if self.q:
            qs = TipoTramite.objects.filter(descripcion__icontains = self.q).exclude(id__in = [1,2,3,4,5,10,11,12,13])
        return qs

class AporteTramiteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        empresa = self.forwarded.get('empresa', None)
        qs = Tramite.objects.filter(empresa = empresa, tipo_tramite__in = [2,3,4,5], status_actividad = True)
        return qs

class AporteTipoTramiteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        
        qs = TipoTramite.objects.exclude(id__in = [1,6,7,8,9,2,3,4,5])
        if self.q:
            qs = TipoTramite.objects.filter(descripcion__icontains = self.q)
        return qs


