from django.views.generic import TemplateView,ListView,DetailView,CreateView,UpdateView,DeleteView
from django.db import IntegrityError, transaction
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy 
from django.db.models import Count, Max, Min, Sum
from django.shortcuts import HttpResponseRedirect, render

####        Clase que Lista Modelos         ###

class GeneralList(ListView):
    filtro_por = None

    def get_queryset(self):
        queryset = self.model.objects.order_by('-id')
        global filtro
        filtro = self.request.GET.get('filtro')
        if filtro:
            filtro = {self.filtro_por : filtro}
            queryset = queryset.filter(**filtro)
        return queryset



####        Clase que Crea Objetos         ###

class GeneralCreate(SuccessMessageMixin, CreateView):
    msg_error = None
    msg_type = None

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():
            try:
                with transaction.atomic():
                    return self.form_valid(form)
            except IntegrityError:
                self.msg_error = 'Error de Integridad en la BD, comuniquese con el Administrador de Base de Datos.'
                return self.form_invalid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        objecto = form.save(commit = False)
        objecto.funcionario = self.request.user
        return super().form_valid(form)

    def form_invalid(self, form):
        if self.msg_type is 'info':
            messages.info(self.request, self.msg_error)
        elif self.msg_type is 'warning':
            messages.warning(self.request, self.msg_error)
        else:
            messages.error(self.request, self.msg_error)
        return HttpResponseRedirect(self.get_success_url())


####        Clase que Edita Objetos         ###

class GeneralUpdate(SuccessMessageMixin, UpdateView):
    editar_por = None
    msg_error = None
    msg_type = None

    def get_object(self, quersyset = None):
        objeto = self.model.objects.get(id = self.kwargs['pk'])
        if self.editar_por:
            filtro = {self.editar_por : self.kwargs['pk']}
            objeto = self.model.objects.get(**filtro)
        return objeto

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST, instance = self.object)
        if form.is_valid():
            try:
                with transaction.atomic():
                    return self.form_valid(form)
            except IntegrityError:
                print(form.errors)
                self.msg_type = 'error'
                self.msg_error = 'Error de Integridad en la BD, comuniquese con el Administrador de Base de Datos.'
                return self.form_invalid(form)
        else:
            return self.form_invalid(form)

    def form_invalid(self, form):
        if self.msg_type is 'info':
            messages.info(self.request, self.msg_error)
        elif self.msg_type is 'warning':
            messages.warning(self.request, self.msg_error)
        else:
            messages.error(self.request, self.msg_error)
        return HttpResponseRedirect(self.get_success_url())


####        Clase que Elimina Objetos         ###

class GeneralDelete(DeleteView):
    eliminar_por = None

    def get_object(self, quersyset = None):
        qs = self.model.objects.get(id = self.kwargs['pk'])
        if self.eliminar_por != None:
            filtro = {self.eliminar_por : self.kwargs['pk']}
            qs = self.model.objects.get(**filtro)
        return qs

    def post(self, request,*args, **kwargs):
        return self.delete(request, *args, **kwargs)
