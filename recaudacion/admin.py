from django.contrib import admin

from .models import *

# Register your models here.


admin.site.register(MetodoPago)
admin.site.register(Banco)
admin.site.register(UnidadTributaria)
admin.site.register(TipoTramite)
admin.site.register(CostoTramite)
admin.site.register(Tramite)
admin.site.register(Agencia)
