
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#j1sah)nxiy#h=_=#^#b53mq0ku+7wp$*ou-8p1f2gek$6r4ek'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1','localhost','192.168.1.41']
INTERNAL_IPS = ('127.0.0.1',)

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'loteria2',
        'USER': 'postgres',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
