from dal import autocomplete
from django.contrib.auth.decorators import login_required, permission_required
from django.urls import path

from .models import *
from .views import *

app_name = 'empresa'

urlpatterns = [

    #########################################################################
    #################### Representantes #####################################
    #########################################################################
    
    path('representante/',
        login_required(ListadoGeneral.as_view(
            model = RepresentanteLegal,
            template_name = 'representantes/representantes_list.html',
            context_object_name = 'representantes',
            extra_context = {'titulo':'Representante Legal','sub_titulo':'Listado General'},
        )),
        name = 'representantes_list'),
    
    path('representante/agregar',
        login_required(RepresentanteCreate.as_view(
            model = RepresentanteLegal,
            form_class = RepresentanteForm,
            success_url = reverse_lazy('empresa:representantes_list'),
            template_name = 'representantes/modal_representante.html',
            success_message = 'Se ha inscrito correctamente el representante legal.',
            extra_context = {'titulo':'Agregar un nuevo Representante',
                            'boton':'Guardar','accion':'save','bg_header':'blue','btn_color':'primary'}
        )),
        name = 'representante_agregar'),
    
    path('representante/edicion/<int:pk>',
        login_required(GeneralUpdate.as_view(
            model = RepresentanteLegal,
            form_class = RepresentanteForm,
            success_url = reverse_lazy('empresa:representantes_list'),
            template_name = 'representantes/modal_representante.html',
            success_message = 'Se han editado correctamente los datos del representante legal.',
            extra_context = {'titulo':'Edición de datos','boton':'Editar','accion':'refresh','bg_header':'blue','btn_color':'primary'},
            msg_error = 'Ha ocurrido un error y sus datos no han sido guardados.',
        )),
        name = 'representante_editar'),

    path('representante/eliminacion/<int:pk>',
        login_required(RepresentanteDelete.as_view(
            model = RepresentanteLegal,
            template_name = 'representantes/modal_representante_eliminacion.html',
            success_url = reverse_lazy('empresa:representantes_list'),
            extra_context = {'titulo':'Eliminación de datos de ','boton':'Eliminar',\
                            'accion':'trash','bg_header':'red'},
        )),
        name = 'representante_delete'),
    
    ### ESTA VISTA HEREDA DE LA CLASE ELIMINAR POR SER BÁSICAMENTE ##
    ###  UN OBJECTO QUE EN EL TEMPLATE MOSTRARÁ SU DETALLE  ## 
    path('representante/detalle/<int:pk>',
        login_required(DetailView.as_view(
            model = RepresentanteLegal,
            template_name = 'representantes/modal_detalle_representante.html',
            extra_context = {'titulo':'Representante Legal ', 'boton':'Ok','accion':'check','bg_header':'navy','btn_color':'info'}
        )),
        name = 'representante_detalle'),
    

    #########################################################################
    ####################### Empresas ########################################
    #########################################################################

    path('sujeto_pasivo/',
        login_required(ListadoGeneral.as_view(
            model = Empresa,
            template_name = 'empresa/empresa_list',
            context_object_name = 'empresas',
            extra_context = {'titulo':'Sujeto Pasivo','sub_titulo':'Listado General'},
        )),
        name = 'sujeto_pasivo_list'),

    ### ESTA VIDA HEREDA DE AGREGARREPRESENTANTE REUTILIZACION DE CÓDIGO ###
    path('sujeto_pasivo/agregar',
        login_required(GeneralCreate.as_view(
            model = Empresa,
            form_class = EmpresaForm,
            success_url = reverse_lazy('empresa:sujeto_pasivo_list'),
            template_name = 'empresa/modal_registro.html',
            extra_context = {'titulo':'Nuevo sujeto pasivo','boton':'Guardar','accion':'save','bg_header':'blue'},
            success_message = 'Se ha inscrito correctamente al sujeto pasivo',
        )),
        name = 'empresa_agregar'),

    path('sujeto_pasivo/edicion/<int:pk>',
        login_required(EditarEmpresa.as_view(
            model = Empresa,
            form_class = EmpresaForm,
            success_url = reverse_lazy('empresa:sujeto_pasivo_list'),
            template_name = 'empresa/modal_registro.html',
            extra_context = {'titulo':'Edición de datos','boton':'Editar','accion':'refresh','bg_header':'blue'},
            success_message = 'Se han editado correctamente los datos del sujeto pasivo.',
            msg_error = 'Ha ocurrido un error y los datos a modificar del Sujeto Pásivo datos no han sido guardados.',
        )),
        name = 'empresa_editar'),

    path('sujeto_pasivo/eliminar/<int:pk>',
        login_required(EliminarEmpresa.as_view(
            model = Empresa,
            template_name = 'empresa/modal_empresa_eliminacion.html',
            success_url = reverse_lazy('empresa:sujeto_pasivo_list'),
            context_object_name = 'empresa',
            extra_context = {'titulo':'Eliminación de datos de','boton':'Eliminar','accion':'trash','bg_header':'red'},
        )),
        name = 'empresa_delete'),
    
    path('sujeto_pasivo/estadistica/<int:annio>',
        login_required(EstadisticaEmpresa.as_view(
            model = Empresa,
            template_name = 'empresa/estadistica.html',
            extra_context = {'titulo':'Estadisticas','sub_titulo':''},
            context_object_name = 'empresas',

        )),
        name = 'estadistica'),
    
    path('sujeto_pasivo/detalle/<int:pk>',
        login_required(EmpresaExpediente.as_view(
            template_name = 'empresa/modal_empresa_detalle.html',
            extra_context = {'titulo':'Sujeto Pasivo','sub_titulo':'Expediente Nro.','boton':'Ok','tipo_btn':'button','accion':'check','bg_header':'navy','btn_color':'info'}
        )),
        name = 'modal_detalle_empresa'),
    
    path('sujeto_pasivo/expediente/<int:pk>',
        login_required(EmpresaExpediente.as_view(
            template_name = 'empresa/EXPEDIENTE/EXPEDIENTE.html',
            extra_context = {'titulo':'Sujeto Pasivo','sub_titulo':'Expediente Nro.',},
            context_object_name = 'empresa',
        )),
        name = 'expediente_empresa'),




##############################################################################
#################### AUTOCOMPLETE'S ##########################################
##############################################################################

    path('ac/representante',
        login_required(RepresentanteAutocomplete.as_view()),
        name = 'autocomplete_representante'),
    path('ac/software',
        login_required(SoftwareAutocomplete.as_view()),
        name = 'autocomplete_software'),
    path('ac/empresa',
        login_required(EmpresaAutocomplete.as_view()),
        name = 'autocomplete_empresa'),
        
]
