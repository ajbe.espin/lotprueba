from dal import autocomplete
### Aqui estan las clases de las cuales Heredan el resto de los modelos ###
from recaudacion.clases import *
from recaudacion.models import *

from .forms import *
from .functions import *
from .models import *

""" LIBRERÍA PROPIA """

# Create your views here.

###     Listado General de Modelos      ###

class ListadoGeneral(GeneralList):
    
    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['titulo'] = self.extra_context['titulo']
        contexto['fecha'] = fecha()
        contexto['registro_annio_pasado'] = self.get_queryset().filter(fecha_registro__year = contexto['fecha']['annio_pasado'])
        contexto['registro_annio_actual'] = self.get_queryset().filter(fecha_registro__year = contexto['fecha']['annio'])
        return contexto


class RepresentanteCreate(GeneralCreate):

    def form_valid(self, form):
        datos = form.save(commit = False)
        datos.funcionario = self.request.user
        datos.save()
        return super().form_valid(form)

### El Update de este Modelo esta en las Url's pues hereda directamente de la clase q esta en recaudacion.clases


#############################################################################################
##          Vista que se encarga de Eliminar de manera génerica objetos al modelo,          #
#############################################################################################
class RepresentanteDelete(GeneralDelete):
        
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user.is_superuser:
            if self.object.empresa_set.count() >= 1:
                self.msg_type = 'info'
                self.msg_error = 'Primero debe eliminar la participación del representante en cada uno de los sujetos pasivos \
                                       en los cuales él tiene participación.'
                messages.info(request, self.msg_error)
                return HttpResponseRedirect(self.get_success_url())
            else:
                messages.success(request,'Los datos han sido eliminados satisfactoriamente.')
                self.object.delete()
                return HttpResponseRedirect(self.get_success_url())
        else:
            self.msg_type = 'error'
            self.msg_error = 'Usted no tiene los permisos necesarios para realizar esta acción.'
            messages.error(request, self.msg_error)
            return HttpResponseRedirect(self.get_success_url())


#########################################################
####################### Empresas ########################
#########################################################



class EditarEmpresa(GeneralUpdate):
    
    def form_valid(self, form):
        datos = form.save(commit = False)
        if self.get_object().representante_legal.count() == 0:
            """
            Aquí garantizo que ningún usuario del sistema deje un sujeto pasivo sin representante legal.
            """
            self.msg_error = 'Usted no cuenta con los permisos necesarios para eliminar todos los Representantes Legales de un \
                                        Sujeto Pasivo. Para realizar esta acción debe contactar al Administrador del Sistema.'
            return self.form_invalid(form)

        if datos.nro_rif != self.get_object().nro_rif or datos.nro_registro_mercantil != self.get_object().nro_registro_mercantil:
            """
            Aquí garantizo que un sujeto pasivo despues de haber adquirido un trámite no pueda cambiar ni su rif ni su registro mercantil 
            """
            if self.get_object().tramite_set.count() >= 1:
                self.msg_error = 'El Sujeto Pasivo tiene Trámites asociados a la numeración de el o los documentos que se han tratado de modificar.'
                return self.form_invalid(form)
            else:
                datos.save()
        return super().form_valid(form)


class EliminarEmpresa(GeneralDelete):

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user.is_superuser:
            if self.object.tramite_set.count() >= 1:
                self.msg_type = 'info'
                self.msg_error = 'Usted no tiene los permisos necesarios para realizar esta eliminación de datos, el sujeto pasivo tiene \
                                    documentos asociados a estas caracteristicas.'
                messages.info(request, self.msg_error)
                return HttpResponseRedirect(self.get_success_url())
            elif self.object.representante_legal.count() == 0:
                self.msg_type = 'warning'
                self.msg_error = 'No se puede dejar un sujeto pasivo sin representante legal.'
                messages.warning(request, self.msg_error)
                return HttpResponseRedirect(self.get_success_url())
            else:
                self.object.delete()
                messages.success(request, 'Se han eliminado los datos del sujeto pasivo.')
                return HttpResponseRedirect(self.get_success_url())
        else:
            self.msg_type = 'error'
            self.msg_error = 'Usted no tiene los permisos necesarios para realizar esta acción.'
            messages.error(request, self.msg_error)
            return HttpResponseRedirect(self.get_success_url())

class EstadisticaEmpresa(ListView):

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        contexto['annio'] = self.kwargs['annio']
        qs = VistaEmpresa.objects.all()
        qsA = qs.filter(annio = contexto['annio'])
        qsB = qs.filter(annio = (contexto['annio']-1))
        """ se invoca a la funcion barra_estadistica() de la libreria externa functions 
        """
        contexto['datos_actuales'] = barra_estadistica(qsA,'total_empresas')
        contexto['datos_pasados'] = barra_estadistica(qsB,'total_empresas')
        """
        Estadisticas mensuales
        """
        contexto['estadistica'] = utilidad(contexto['datos_actuales'])
        contexto['mes'] = reemplazo_mes(contexto['estadistica']['mes'])

        return contexto

class EmpresaExpediente(DetailView):
    model = Empresa

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        """ Funcion zfill, utilizada para dar formato al id en la vista"""
        contexto['nro_expediente'] = str(self.get_object().id).zfill(5)

        """ QS que muestra los pagos realizados por la empresa """
        contexto['servicios'] = self.get_object().tramite_set.all()
        """ Últimos 5 pagos """
        contexto['sum_5_pagos'] = contexto['servicios'][:5].aggregate(suma = Sum('monto_bauche'))['suma']
        contexto['sum_5_pagos_soberanos'] = contexto['sum_5_pagos'] / 100000
        
        """ TRAMITES
            La clausula __in me permite incorporar una lista dentro de un filter
         """
        contexto['mis_tramites'] = Tramite.objects.all()
        contexto['inscripciones'] = contexto['mis_tramites'].filter(empresa = self.get_object(), tipo_tramite = 1) # inscripciones
        contexto['licencias'] = contexto['mis_tramites'].filter(empresa = self.get_object(), tipo_tramite__in = [2,3,4,5])
        return contexto


                
#########################################################
#################### AUTOCOMPLETE'S #####################
#########################################################



class RepresentanteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = RepresentanteLegal.objects.none()
        if self.q:
            qs = RepresentanteLegal.objects.filter(persona__cedula_identidad__icontains = self.q)
        return qs

class SoftwareAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Software.objects.none()
        if self.q:
            qs = Software.objects.filter(software__icontains = self.q)
        return qs

class EmpresaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Empresa.objects.none()
        if self.q:
            qs = Empresa.objects.filter(nro_rif__icontains = self.q)
        return qs
