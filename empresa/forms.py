import datetime
import re

from dal import autocomplete
from django import forms

from .models import *


class RepresentanteForm(forms.ModelForm):
    class Meta:
        model = RepresentanteLegal
        fields = [
            'persona',
            'email',
            'telefono',
            'celular',
            'firma_personal',
            'persona_contacto',
        ]

        widgets = {
            'persona':autocomplete.ModelSelect2(url='cuenta:autocomplete_persona', attrs={'class': 'form-control','data-placeholder': '..... CEDULA DE IDENTIDAD .....', 'style':'width:100%'},),
            'email':forms.EmailInput(attrs = {'class':'form-control','data-placeholher':'Email'}),
            'telefono':forms.TextInput(attrs = {'class':'form-control','data-placeholher':'Nro Telefono Local'}),
            'celular':forms.TextInput(attrs = {'class':'form-control','data-placeholher':'Nro Celular'}),
            'firma_personal':forms.CheckboxInput(attrs = {'class':'minimal','style':'width:100%', 'placeholder':'Firma Personal'}),
            'persona_contacto':forms.CheckboxInput(attrs = {'class':'minimal','style':'width:100%', 'placeholder':'Firma Personal'}),
        
        }


    def clean_telefono(self):
        """
        Validamos que el teléfono cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        telefono = diccionario_limpio.get('telefono')
        patron = re.compile('^\+58\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$')
        if telefono:
            if patron.match(telefono) is None:
                raise forms.ValidationError("El número de teléfono local debe\
                                                cumplir con la forma +58 (999)\
                                                999-99-99")
        return telefono

    def clean_celular(self):
        """
        Validamos que el celular cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        celular = diccionario_limpio.get('celular')
        patron = re.compile('^\+58\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$')
        if celular:
            if patron.match(celular) is None:
                raise forms.ValidationError(
                    "El número de teléfono celular debe cumplir con la forma +58 (999) 999-99-99")
        return celular

    def clean_direccion(self):
        diccionario_limpio = self.cleaned_data
        direccion = diccionario_limpio.get('direccion')
        direccion.upper()
        return celular



############################################################################


class EmpresaForm(forms.ModelForm):
    class Meta:
        model = Empresa
        fields = [
            'representante_legal',
            'razon_social',
            'nombre_comercial',
            'nro_rif',
            'nro_registro_mercantil',
            'telefono',
            'otro_telefono',
            'email',
            'segundo_email',
            'domicilio_fiscal',
            'zona_postal',
            'software',
        ]

        widgets = {
            'representante_legal':autocomplete.ModelSelect2Multiple(url='empresa:autocomplete_representante', attrs={'class': 'form-control','data-placeholder': '..... Inscripción SDLC .....', 'multiple':'multiple', 'style':'width:100%'},),
            'razon_social':forms.TextInput(attrs = {'class':'form-control','placeholder':'Razón Social'}),
            'nombre_comercial':forms.TextInput(attrs = {'class':'form-control','placeholder':'Nombre Comercial'}),
            'nro_rif':forms.TextInput(attrs = {'class':'form-control'}),
            'nro_registro_mercantil':forms.TextInput(attrs = {'class':'form-control'}),
            'telefono':forms.TextInput(attrs = {'class':'form-control'}),
            'otro_telefono':forms.TextInput(attrs = {'class':'form-control'}),
            'email':forms.EmailInput(attrs = {'class':'form-control','placeholder':'Em@il 1'}),
            'segundo_email':forms.EmailInput(attrs = {'class':'form-control','placeholder':'Em@il 2'}),
            'domicilio_fiscal':forms.TextInput(attrs = {'class':'form-control','placeholder':'Domicilio Fiscal'}),
            'zona_postal':autocomplete.ModelSelect2(url='cuenta:autocomplete_zona_postal', attrs={'data-placeholder': '..... Zona Postal .....','class': 'form-control', 'style':'width:100%'},),
            'software':autocomplete.ModelSelect2Multiple(url='empresa:autocomplete_software', attrs={'data-placeholder': '..... Software SDLC .....', 'class': 'form-control', 'multiple':'multiple', 'style':'width:100%'},),
            
        }


    def clean_nro_rif(self):
        """
        Validamos que el teléfono cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        nro_rif = diccionario_limpio.get('nro_rif')
        patron = re.compile('^[GgJjVvEe]\-\d{5,8}\-\d{1}$')
        if nro_rif:
            if patron.match(nro_rif) is None:
                raise forms.ValidationError("El número de número de rif debe\
                                            cumplir con la forma J-12345678-9\
                                            , Letras Válidas G-J-V-E.")
        nro_rif = nro_rif.upper()
        return nro_rif

    def clean_telefono(self):
        """
        Validamos que el teléfono cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        telefono = diccionario_limpio.get('telefono')
        patron = re.compile('^\+58\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$')
        if telefono:
            if patron.match(telefono) is None:
                raise forms.ValidationError("El número de teléfono local debe\
                                                cumplir con la forma +58 (999)\
                                                999-99-99")
        return telefono

    def clean_otro_telefono(self):
        """
        Validamos que el celular u otro telefono adicional cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        otro_telefono = diccionario_limpio.get('otro_telefono')
        patron = re.compile('^\+58\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$')
        if otro_telefono:
            if patron.match(otro_telefono) is None:
                raise forms.ValidationError(
                    "El número de teléfono móvil debe \
                    cumplir con la forma +58 (999) 999-99-99")
        return otro_telefono

    def clean_registro_mercantil(self):
        """
        Validamos que el clean_registro_mercantil cumpla con el formato
        """
        diccionario_limpio = self.cleaned_data
        nro_registro_mercantil = diccionario_limpio.get('nro_registro_mercantil')
        patron = re.compile('^\d{3}\-\{5}$')
        if nro_registro_mercantil:
            if patron.match(nro_registro_mercantil) is None:
                raise forms.ValidationError(
                    "El número de registro mercantil debe \
                    cumplir con la forma 999-99999")
        return nro_registro_mercantil


############################################################################
