import time
from datetime import date, datetime, timedelta

from django.db.models import Count, Max, Min, Sum

from recaudacion.models import *

from .models import *


def fecha():
    hoy = datetime.now()
    fechas = {
        'hoy':hoy,
        'fecha':hoy.strftime('%d de %B de %Y'),
        'annio':hoy.year,
        'mes':hoy.month,
        'annio_pasado':((hoy.year) - 1),
    }
    return fechas

def barra_estadistica(qs,field):
    """ la funcion nos renderiza en el template la lista con los valores solicitados en el párametro
    """
    datos = [0,0,0,0,0,0,0,0,0,0,0,0]

    for i in qs:
        a = 'i.'+ field
        param = eval(a)
        datos[int(i.mes)-1] = int(param)

    return datos

def reemplazo_mes(num):
    """ la funcion nos reemplaza en el template su equivalente en caracter de los meses de año
    """
    mes = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',\
         'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
    return mes[num - 1]      

def utilidad(lista):
    """
        La función nos retorna un diccionario con las estadisticas de una lista
        que debe ser una lista de valores por mes de X operacion
    """

    """ Este obtiene el mayor valor de la lista """
    mayor_valor_lista = max(lista)
    """ Se le aumenta 1 debido a que los meses inician desde 1, las listas en 0 """
    indice_lista = lista.index(mayor_valor_lista) + 1

    report = {
        'mes':indice_lista,
        'cantidad':mayor_valor_lista,
        'soberano':mayor_valor_lista/100000,
    }
    return report

def costo_tramite(tramite):
    ut = UnidadTributaria.objects.all().aggregate(total = Max('unidad_tributaria'))['total']
    costo = CostoTramite.objects.filter(tipo_tramite = tramite).aggregate(total = Max('monto'))['total']
    return ut * costo

def credito_fiscal(empresa, concepto, tramite_instance, monto):
    CreditoFiscal.objects.create(
                                empresa = empresa,
                                concepto = concepto,
                                tramite = tramite_instance,
                                ingreso = monto
                            )
