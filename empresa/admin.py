from django.contrib import admin

from .models import *

# Register your models here.

admin.site.register(RepresentanteLegal)
admin.site.register(Empresa)
admin.site.register(Software)
admin.site.register(CreditoFiscal)
