from django.db import models
from django.db.models import Avg, Count, Max, Sum

# Create your models here.

class RepresentanteLegal(models.Model):
    persona = models.OneToOneField('cuenta.Saime', null = False, blank = False, on_delete = models.PROTECT, db_column = 'persona_id', related_name = 'datos_personales', unique = True)
    email = models.EmailField(unique = True)
    telefono = models.CharField(max_length=255, blank=True, null=True)
    celular = models.CharField(max_length=255, blank=True, null=True)
    fecha_registro = models.DateField(auto_now_add = True)
    firma_personal = models.BooleanField(default = False)
    persona_contacto = models.BooleanField(default = False)
    funcionario = models.ForeignKey('cuenta.Persona', null = False, blank = False, on_delete = models.PROTECT, db_column = 'funcionario_id', related_name = 'funcionario_resp')
    status = models.BooleanField(default = True)
	
    def __str__(self):
    	return '%s'%(self.persona)


    class Meta:
	    db_table = 'empresa\".\"representante_legal'
	    verbose_name = "Representante Legal"
	    verbose_name_plural = "Representantes Legales"
	    ordering = ['-id']

class Empresa(models.Model):
	representante_legal = models.ManyToManyField('RepresentanteLegal', blank = False)
	nro_registro_mercantil = models.CharField(max_length = 9, unique = True)
	nro_rif = models.CharField(max_length = 12, unique = True)
	razon_social = models.CharField(max_length = 80)
	nombre_comercial = models.CharField(max_length = 80)
	telefono = models.CharField(max_length = 19, blank = True)
	otro_telefono = models.CharField(max_length = 19, blank = True)
	email = models.EmailField(unique = True)
	segundo_email = models.EmailField(blank = True)
	domicilio_fiscal = models.CharField(max_length = 250)
	zona_postal = models.ForeignKey('cuenta.ZonaPostal', null = True, blank = True, on_delete = models.PROTECT, verbose_name = 'Zona postal', db_column = 'zona_postal_id', related_name = 'zona_id')
	fecha_registro = models.DateField(auto_now_add = True)
	software = models.ManyToManyField('Software')
	saldo = models.FloatField(default = 0)
	funcionario = models.ForeignKey('cuenta.Persona',
									null = False,
									blank = False,
									on_delete = models.PROTECT,
									db_column = 'funcionario_id',
									related_name = 'funcionario_sdlc')
	def __str__(self):
		return '%s'%(self.nro_rif)

	
	class Meta:
		db_table = 'empresa\".\"empresa'
		verbose_name = "Empresa"
		verbose_name_plural = "Empresas"
		ordering = ['-id']


class Software(models.Model):
	software = models.CharField(max_length = 30)
	descripcion = models.CharField(max_length = 100)
	fecha_alta = models.DateField(auto_now_add = True)

	def __str__(self):
		return '%s'%(self.software)
        
	class Meta:
		db_table = 'empresa\".\"software'
		verbose_name = "Software"
		verbose_name_plural = "Software"
		ordering = ['-id']


class CreditoFiscal(models.Model):
	empresa = models.ForeignKey('Empresa', models.DO_NOTHING, blank=False, null=False)
	concepto = models.TextField(null = True, blank = True)# concepto de egreso o ingeso (deposito o algun tramite)
	tramite = models.OneToOneField('recaudacion.Tramite', on_delete = models.PROTECT, null = True, blank = True, db_column='pago_id')
	ingreso = models.FloatField(default = 0)
	egreso = models.FloatField(default = 0)

	def __str__(self):
		return '%s'%(self.id)

	@property
	def ingreso_soberano(self):
		soberano = self.ingreso/100000
		return soberano
        
	class Meta:
		db_table = 'empresa\".\"credito_fiscal'
		verbose_name = "Crédito Fiscal"
		verbose_name_plural = "Créditos Fiscales"
		ordering = ['-id']

		
############################################################
################### VISTAS POSTGRES ########################
############################################################


class VistaEmpresa(models.Model):
	id = models.IntegerField(primary_key=True)
	mes = models.IntegerField()
	annio = models.IntegerField()
	total_empresas = models.IntegerField()

	class Meta:
		managed = False
		db_table = 'tools\".\"empresas_view'

# class VistaExpediente(models.Model):
# 	id = models.IntegerField(primary_key = True)
# 	empresa_id = models.IntegerField()
# 	razon_social = models.CharField(max_length = 80)
# 	nombre_comercial = models.CharField(max_length = 80)
# 	nro_rif = models.CharField(max_length = 12)
# 	id_pago = models.IntegerField()
# 	descripcion = models.CharField(max_length = 40)
# 	deposito_bsf = models.IntegerField()
# 	deposito_bss = models.FloatField()
# 	costo_bsf = models.IntegerField()
# 	costo_bss = models.FloatField()
# 	fecha_emision = models.DateField()
# 	username = models.CharField(max_length = 150)
# 	responsable = models.TextField()
	
# 	class Meta:
# 		managed = False
# 		db_table = 'tools\".\"expediente_empresa'